from calculator import *
from todo import *


def create_calculator():
    calcul = Toplevel(root)
    app = Calculator(calcul)
    app.pack()
    position_wind(calcul)
    app.mainloop()


def create_todo():
    todo_list = Toplevel(root)
    app = ToDo(todo_list)
    app.pack()
    position_win(todo_list)
    app.mainloop()

root = Tk()
root.title('Widgets')
Style().configure("TButton", padding=(0, 5, 0, 5), font='serif 10', background='#04e2df', activebackground = 'red')
root.config(background='#a4d4d3')
td = Button(root, text="Todo - list", command=create_todo)
td.grid(column=0, row=0, pady=10, padx=10)
calc = Button(root, text="Calculator", command=create_calculator)
calc.grid(column=1, row=0, pady=10, padx=10)

position_wind(root)

root.mainloop()

