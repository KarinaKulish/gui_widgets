from tkinter import *
from tkinter.ttk import *
from calculation import calculate


class Calculator(Frame):
    def __init__(self, parent):
        Frame.__init__(self, parent)
        self.parent = parent
        self.gui()

    def gui(self):
        self.parent.title("Calculator")

        Style().configure("TButton", padding=(0, 5, 0, 5),
                          font='serif 10', background='#04e2df', activebackground = 'red')

        Style().configure("TFrame", background='#a4d4d3')

        buttons = ['7', '8', '9', '4', '6', '5', '1', '2', '3', '.', '0', '=']
        buttons_sign = ['DEL', '/', '*', '-', '+']
        b_signs_more = ['M+', 'M-', 'sin', 'cos', 'log', 'ln', 'tg', '√', '!', 'π', 'e', '^', '(', ')']
        inv = ['M+', 'M-', 'sin^-1', 'cos^-1', '10^x', 'e^x', 'tg^-1', 'x^2', '!', 'π', 'e', '^', '(', ')']

        for i in range(4):
            self.columnconfigure(i, pad=5)

        for i in range(6):
            self.rowconfigure(i, pad=10)

        base_frame = Frame(self)
        base_frame.grid(row=1, column=3, rowspan=5)
        more_frame = Frame(self)
        more_frame.grid(row=1, column=4, rowspan=5)
        adv_frame = Frame(self)

        entry = Entry(self)
        entry.grid(row=0, columnspan=5, sticky=W + E, pady=10, padx=5, ipady=5)
        entry.config(font='serif 15')

        r1 = 1
        c1 = 0
        for i in buttons:
            cmd1 = lambda x=i: calculate(x, entry)
            button(self, i, cmd1, r1, c1)
            c1 += 1
            if c1 > 2:
                c1 = 0
                r1 += 1

        r2 = 0
        for i in buttons_sign:
            cmd2 = lambda x=i: calculate(x, entry)
            button(base_frame, i, cmd2, r2, 0)
            r2 += 1

        def more():
            entry.grid(row=0, columnspan=7, sticky=W + E, pady=10, padx=5, ipady=5)
            adv_frame.grid(row=1, column=5, rowspan=5)
            r3 = 0
            c3 = 1
            for i in b_signs_more:
                cmd3 = lambda x=i: calculate(x, entry)
                button(adv_frame, i, cmd3, r3, c3)
                c3 += 1
                if c3 > 2:
                    c3 = 0
                    r3 += 1

            more_btn.grid_remove()
            more_btn_hide.grid(column=0, row=0, ipady=60, padx=5)

        def more_hide():
            adv_frame.grid_forget()
            more_btn_hide.grid_remove()
            more_btn.grid()

        more_btn = Button(more_frame, text='>', command=more)
        more_btn.grid(column=0, row=0, ipady=60, padx=5)
        more_btn.config(width=2)

        more_btn_hide = Button(more_frame, text='<', command=more_hide)
        more_btn_hide.config(width=2)

        def inv1():
            r4 = 0
            c4 = 1
            for i in inv:
                cmd4 = lambda x=i: calculate(x, entry)
                button(adv_frame, i, cmd4, r4, c4)
                c4 += 1
                if c4 > 2:
                    c4 = 0
                    r4 += 1

            inverse_1.grid_remove()
            inverse_2.grid(column=0, row=0)

        def inv2():
            inverse_2.grid_remove()
            inverse_1.grid()
            r5 = 0
            c5 = 1
            for i in b_signs_more:
                cmd5 = lambda x=i: calculate(x, entry)
                button(adv_frame, i, cmd5, r5, c5)
                c5 += 1
                if c5 > 2:
                    c5 = 0
                    r5 += 1

        inverse_1 = Button(adv_frame, text='INV', command=inv1)
        inverse_1.grid(column=0, row=0)
        inverse_2 = Button(adv_frame, text='INV-', command=inv2)


def button(frame, tex, comm, r, c):
    bt = Button(frame, text=tex, command=comm)
    bt.grid(row=r, column=c)


def position_wind(fr):
    x = (fr.winfo_screenwidth() / 2 - fr.winfo_reqwidth())
    y = (fr.winfo_screenheight() / 2 - fr.winfo_reqheight())
    fr.wm_geometry("+%d+%d" % (x, y))

'''
def main():
    root = Tk()
    app = Calculator(root)
    app.pack()
    position_win(root)
    app.mainloop()


if __name__ == '__main__':
    main()'''

