from tkinter import *
from tkinter.ttk import *
from tkinter import messagebox


class ToDo(Frame):
    def __init__(self, head):
        Frame.__init__(self, head)
        self.head = head
        self.todo()

    def todo(self):
        self.head.title("ToDo")

        Style().configure("TButton", padding=(0, 5, 0, 5), font='serif 10', background='#04e2df')
        Style().configure("TLabel", font='serif 11', background='#a4d4d3')
        Style().configure("TFrame", background='#a4d4d3')

        todo_list = Listbox(self, width=40, bg='white', font='serif 11')
        todo_list.grid(row=2, column=0, padx=10, columnspan=4, pady=5, rowspan=10)

        prior_combo = Combobox(self, values=['Low', 'Medium', 'High'], width=6, font='serif 11')
        prior_combo.grid(row=0, column=1, padx=5, ipady=2)

        lbl_prior = Label(self, text='Priority')
        lbl_prior.grid(row=0, column=0, padx=5, pady=15)

        entry = Entry(self, font='serif 12')
        entry.grid(row=0, column=3, padx=5, ipady=2)

        def get_text():
            task = entry.get()
            pr = prior_combo.get()
            if task != '':
                if pr != '':
                    if pr == "Low":
                        todo_list.insert(END, task)
                        todo_list.itemconfig(END, background='#9ef61f')
                    elif pr == 'Medium':
                        todo_list.insert(END, task)
                        todo_list.itemconfig(END, background='#f7dc6f')
                    elif pr == "High":
                        todo_list.insert(END, task)
                        todo_list.itemconfig(END, background='#ec7063')
                    else:
                        todo_list.insert(END, task)
                        todo_list.itemconfig(END, background='#bb8fce')
                else:
                    todo_list.insert(END, task)
                    todo_list.itemconfig(END, background='#58d68d')
            else:
                messagebox.showwarning("Warning", 'Enter a task', parent=self)
            entry.delete(0, END)
            prior_combo.delete(0, END)

        def done():
            index = todo_list.curselection()
            r = todo_list.get(index)
            done_list.insert(END, r)
            todo_list.delete(index)

        def dell():
            index = done_list.curselection()
            done_list.delete(index)

        def del_all_todo():
            confirmed = messagebox.askyesno("Delete", "Are u sure you want to delete all tasks in Todo?", parent=self)
            if confirmed:
                todo_list.delete(0, END)

        def del_all_done():
            confirmed = messagebox.askyesno("Delete", "Are u sure you want to delete all tasks in Done?", parent=self)
            if confirmed:
                done_list.delete(0, END)

        bt_add = Button(self, command=get_text, text='Add')
        bt_add.grid(row=0, column=4, padx=5,columnspan=1)

        bt_todo_del = Button(self, command=del_all_todo, text='Delete TODO')
        bt_todo_del.grid(row=4, column=4, padx=5,columnspan=1)

        bt_done_del = Button(self, command=del_all_done, text='Delete DONE')
        bt_done_del.grid(row=16, column=4, padx=5, columnspan=1)

        bt_done = Button(self, command=done, text='Done')
        bt_done.grid(row=2, column=4, padx=5,columnspan=1)

        lbl_todo = Label(self, text='TODO')
        lbl_todo.grid(row=1, column=0)

        lbl_done = Label(self, text='DONE')
        lbl_done.grid(row=13, column=0, pady=10)

        done_list = Listbox(self, width=40, bg='white', font='serif 11', foreground='red')
        done_list.grid(row=14, column=0, padx=10, columnspan=4, pady=10, rowspan=10)

        bt_del = Button(self, command=dell, text='Delete')
        bt_del.grid(row=14, column=4, padx=5)


def position_win(fr):
    x = (fr.winfo_screenwidth() / 2 - fr.winfo_reqwidth())
    y = (fr.winfo_screenheight() / 3 - fr.winfo_reqheight())
    fr.wm_geometry("+%d+%d" % (x, y))

'''
if __name__ == '__main__':
    root = Tk()
    app = ToDo(root)
    app.pack()
    position_win(root)
    app.mainloop()'''
