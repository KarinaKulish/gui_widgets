from tkinter import *
from tkinter import messagebox
import math


m = 0


def calculate(sign, frm):
    global m
    if sign == "=":
        try:
            result = eval(frm.get())
            frm.delete(0, END)
            frm.insert(END, str(result))
        except:
            frm.delete(0, END)
            frm.insert(END, "Error!")
            messagebox.showerror("Error!", "Invalid data")

            # очищение поля ввода
    elif sign == "DEL":
        frm.delete(0, END)

    elif sign == "π":
        frm.insert(END, math.pi)

    elif sign == "^":
        frm.insert(END, '**')
    elif sign == 'sin':
        res = frm.get()
        frm.delete(0, END)
        frm.insert(END, str(math.sin(float(res))))
    elif sign == "cos":
        res = frm.get()
        frm.delete(0, END)
        frm.insert(END, str(math.cos(float(res))))
    elif sign == "(":
        frm.insert(END, "(")
    elif sign == ")":
        frm.insert(END, ")")
    elif sign == "!":
        res = frm.get()
        frm.delete(0, END)
        frm.insert(END, str(math.factorial(int(res))))
    elif sign == "√":
        res = frm.get()
        frm.delete(0, END)
        frm.insert(END, str(math.sqrt(float(res))))
    elif sign == "e":
        frm.insert(END, math.e)
    elif sign == "log":
        res = frm.get()
        frm.delete(0, END)
        frm.insert(END, str(math.log10(float(res))))
    elif sign == "ln":
        res = frm.get()
        frm.delete(0, END)
        frm.insert(END, str(math.log(float(res))))
    elif sign == "tg":
        res = frm.get()
        frm.delete(0, END)
        frm.insert(END, str(math.tan(float(res))))

    elif sign == "sin^-1":
        try:
            res = frm.get()
            frm.delete(0, END)
            frm.insert(END, str(math.asin(float(res))))
        except ValueError:
            messagebox.showerror("Value Error!", " Invalid input  ")

    elif sign == "cos^-1":
        try:
            res = frm.get()
            frm.delete(0, END)
            frm.insert(END, str(math.acos(float(res))))
        except ValueError:
            messagebox.showerror("Value Error!", " Invalid input  ")

    elif sign == "10^x":
        res = frm.get()
        frm.delete(0, END)
        frm.insert(END, str(math.pow(10, float(res))))

    elif sign == "e^x":
        res = frm.get()
        frm.delete(0, END)
        frm.insert(END, str(math.exp(float(res))))
    elif sign == "tg^-1":
        res = frm.get()
        frm.delete(0, END)
        frm.insert(END, str(math.atan(float(res))))
    elif sign == "x^2":
        res = frm.get()
        frm.delete(0, END)
        frm.insert(END, str(math.pow(float(res), 2)))
    elif sign == "M+":
        m = frm.get()
        print(m)
    elif sign == "M-":
        frm.insert(END, m)
    else:
        if "=" in frm.get():
            frm.delete(0, END)
        frm.insert(END, sign)